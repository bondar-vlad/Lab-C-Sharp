﻿using System;

namespace Lab_C_Sharp
{
    public class Person
    {
        private string name;
        private string surname;
        private DateTime dateofbirth;

        public string Name
        {
            get => name;
            set => name = value;
        }

        public string Surname
        {
            get => surname;
            set => surname = value;
        }

        public DateTime DateOfBirth
        {
            get => dateofbirth;
            set => dateofbirth = value;
        }

        public Person(string name, string surname, DateTime dateofbirth)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.surname = surname ?? throw new ArgumentNullException(nameof(surname));
            this.dateofbirth = dateofbirth;
        }

        public Person()
        {
            name = "";
            surname = "";
            dateofbirth = new DateTime();
        }

        public override string ToString()
        {
            return $"\tИмя: {Surname}\n\tФамилия: {Name}\n\tДата рождения: {DateOfBirth.ToShortDateString()}";
        }
        
        public string ToShortString()
        {
            return Name + " " + Surname;
        }

        public object DeepCopy()
        {
            object person = new Person(Name, Surname, DateOfBirth);
            return person;
        }
        public override bool Equals(object obj)
        {
            return obj != null && this.ToString() == obj.ToString();
        }
        public static bool operator ==(Person p1, Person p2)
        {
            return p1.Equals(p2);
        }
        public static bool operator !=(Person p1, Person p2)
        {
            return !p1.Equals(p2);
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}