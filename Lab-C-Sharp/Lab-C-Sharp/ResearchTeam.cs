﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Lab_C_Sharp
{
    public class ResearchTeam : Team, INameAndCopy, IComparer<ResearchTeam>, INotifyPropertyChanged
    {
        private string subject;
        private TimeFrame duration;
        private List<Person> listofmembers;
        private List<Paper> listofpapers;

        public string Subject
        {
            get => subject;
            set => subject = value;
        }

        public TimeFrame Duration
        {
            get => duration;
            set => duration = value;
        }

        public List<Person> ListOfMembers
        {
            get => listofmembers;
            set => listofmembers = value;
        }

        public List<Paper> ListOfPapers
        {
            get => listofpapers;
            set => listofpapers = value;
        }
        public Team TeamCopy
        {
            get => (Team)DeepCopy();
            set
            {
                Name = value.Name;
                Id = value.Id;
            }
        }

        public ResearchTeam(string subject, string name, int id,  TimeFrame duration) : base(name, id)
        {
            this.subject = subject ?? throw new ArgumentNullException(nameof(subject));
            this.duration = duration;
            ListOfPapers = new List<Paper>();
            ListOfMembers = new List<Person>();
        }
        
        public ResearchTeam()
        {
            Subject = "";
            Duration = TimeFrame.Year;
            ListOfPapers = new List<Paper>();
            ListOfMembers = new List<Person>();
        }
        public Paper LastPaper
        {
            get
            { return ListOfPapers.Count > 0 ? ListOfPapers.First(x => x.Date == ListOfPapers.Max(y => y.Date)) : null; }
        }
        public void AddPapers(params Paper[] papers)
        {
            ListOfPapers.AddRange(papers);
        }
        public void AddMembers(params Person[] members)
        {
            ListOfMembers.AddRange(members);
        }
        public override string ToString()
        {
            var stringOfPapers = ListOfPapers.Aggregate("", (current, p) => current + ("\n***\n" + p.ToString()));
            var stringOfPersons = ListOfMembers.Aggregate("", (current, p) => current + ("\n***\n" + p.ToShortString()));
            return
                $"Предмет: {Subject}\nОрганизация: {Name}\nНомер: {Convert.ToString(Id)}\nДлительность: {Convert.ToString(Duration)}\nСписок публикаций: {stringOfPapers}\nСписок учасников: {stringOfPersons}";
        }
        public string ToShortString()
        {
            return
                $"Предмет: {Subject}\nОрганизация: {Name}\nНомер: {Convert.ToString(Id)}\nДлительность: {Convert.ToString(Duration)}";
        }
        public override object DeepCopy()
        {
            object rt = new ResearchTeam(Subject, Name, Id, Duration);
            foreach(Paper p in ListOfPapers)
            {
                ((ResearchTeam)rt).AddPapers((Paper)(p.DeepCopy()));
            }
            foreach (Person p in ListOfMembers)
            {
                ((ResearchTeam)rt).AddMembers((Person)(p.DeepCopy()));
            }
            return rt;
        }
        public IEnumerable GetBadMembers()
        {
            return ListOfMembers.Where(p => !ListOfPapers.Exists(x => (x.Author == p)));
        }
        public IEnumerable GetPapers(int n)
        {
            return ListOfPapers.Where(p => DateTime.Today.Year - p.Date.Year <= n);
        }
        public int Compare(ResearchTeam t1, ResearchTeam t2)
        {
            if(t1 != null && t2 != null)
            {
                return t1.Subject.CompareTo(t2.Subject);
            }
            throw new ArgumentException("Does not exist one of param");
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}