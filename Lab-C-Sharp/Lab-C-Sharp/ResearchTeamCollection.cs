﻿using System;
using System.Collections.Generic;

namespace Lab_C_Sharp
{
    public class ResearchTeamCollection<TKey>
    {
        private string Name { get; set; }
        private List<ResearchTeam> ResearchTeamList;
        public Dictionary<TKey, ResearchTeam> ResearchTeamDictionary;
        
        public void AddDefaults()
        {
            if (ResearchTeamList == null)
            {
                ResearchTeamList = new List<ResearchTeam>();
            }
            ResearchTeamList.Add(new ResearchTeam());
        }
        public void AddResearchTeams(params ResearchTeam[] arr)
        {
            if (ResearchTeamList == null)
            {
                ResearchTeamList = new List<ResearchTeam>();
            }
            ResearchTeamList.AddRange(arr);
        }
        public override string ToString()
        {
            string s = "";
            foreach(ResearchTeam rt in ResearchTeamList)
            {
                s += rt + "\n***\n";
            }
            return s;
        }
        public virtual string ToShortString()
        {
            string s = "";
            foreach(ResearchTeam rt in ResearchTeamList)
            {
                s += rt.ToShortString();
                s +=
                    $"\nЧисло участников проекта: {rt.ListOfMembers.Count}\nЧисло публикаций: {rt.ListOfPapers.Count}\n";
            }
            return s;
        }
        public void SortById()
        {
            ResearchTeamList.Sort();
        }
        public void SortBySubject()
        {
            ResearchTeamList.Sort(new ResearchTeam());
        }
        public void SortByNumberOfPapers()
        {
            ResearchTeamList.Sort(new ResearchTeamNumberOfPapersComparer());
        }
        public ResearchTeamCollection(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public ResearchTeamCollection()
        {
            Name = "";
        }
    }

    public class ResearchTeamNumberOfPapersComparer : IComparer<ResearchTeam>
    {
        public int Compare(ResearchTeam t1, ResearchTeam t2)
        {
            if (t1 != null && t2 != null)
            {
                return t1.ListOfPapers.Count.CompareTo(t2.ListOfPapers.Count);
            }
            throw new ArgumentException("Does not exist one of param");
        }
    }
}