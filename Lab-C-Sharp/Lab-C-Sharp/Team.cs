﻿using System;

namespace Lab_C_Sharp
{
    public class Team : IComparable
    {
        protected string name;
        protected int id;
        public string Name
        {
            get => name;
            set => name = value;
        }
        public int Id
        {
            get => id;
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("Id (в экземпляре класса Team)");
                }
                id = value;
            }
        }

        public Team(string name, int id)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.id = id;
        }

        public Team()
        {
            Name = "";
            Id = 0;
        }
        public override string ToString()
        {
            return $"Название организации: {Name}\nНомер организации: {Convert.ToString(Id)}";
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            return ToString() == obj.ToString();
        }
        public static bool operator ==(Team t1, Team t2)
        {
            return t1.Equals(t2);
        }
        public static bool operator !=(Team t1, Team t2)
        {
            return !t1.Equals(t2);
        }
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
        public virtual object DeepCopy()
        {
            object team = new Team(Name, Id);
            return team;
        }
        int IComparable.CompareTo(object obj)
        {
            Team temp = obj as Team;
            if(temp != null)
            {
                return Id.CompareTo(temp.Id);
            }
            throw new ArgumentException("Param not Team");
        }
    }
}