﻿namespace Lab_C_Sharp
{
    public interface INameAndCopy
    {
        string Name { get; set; }
        object DeepCopy();
    }
}