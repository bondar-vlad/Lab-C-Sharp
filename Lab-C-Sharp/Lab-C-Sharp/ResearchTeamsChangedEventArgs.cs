﻿using System;

namespace Lab_C_Sharp
{
    public class ResearchTeamsChangedEventArgs<TKey> : System.EventArgs
    {
        public string Name { get; set; }
        public Revision Info { get; set; }
        public string Source { get; set; }
        public int Id { get; set; }

        public ResearchTeamsChangedEventArgs(string name, Revision info, string source, int id)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Info = info;
            Source = source ?? throw new ArgumentNullException(nameof(source));
            Id = id;
        }

        public override string ToString()
        {
            return $"Название коллекции - {Name}\nСобытие было вызвано после {Info}\nИсточник изменения - {Source}\nНомер регистрации обьекта - {Id}\n";
        }
    }
}