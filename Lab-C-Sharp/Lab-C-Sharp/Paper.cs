﻿using System;

namespace Lab_C_Sharp
{
    public class Paper
    {
        public string Name { get; set; }
        public Person Author { get; set; }
        public DateTime Date { get; set; }

        public Paper(string name, Person author, DateTime date)
        {
            if (author == null) throw new ArgumentNullException(nameof(author));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Author = author;
            Date = date;
        }

        public Paper()
        {
            Name = "";
            Author = new Person();
            Date = new DateTime();
        }
        public override string ToString()
        {
            return $"Имя: {Name}\nАвтор:\n{Author}\nДата публикации: {Date.ToShortDateString()}";
        }
        public object DeepCopy()
        {
            object paper = new Paper(Name, Author.DeepCopy() as Person, Date);
            return paper;
        }
    }
}