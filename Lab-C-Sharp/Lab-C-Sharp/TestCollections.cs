﻿using System;
using System.Collections.Generic;

namespace Lab_C_Sharp
{
    public class TestCollections
    {
        private static System.Collections.Generic.List<Team> TestTeamList;
        private static System.Collections.Generic.List<string> TestStringList;
        private static System.Collections.Generic.Dictionary<Team,ResearchTeam> TestTeamDictionary;
        private static System.Collections.Generic.Dictionary<string,ResearchTeam> TestStringDictionary;
        public static ResearchTeam TestStaticMethod(int n)
        {
            ResearchTeam rt = new ResearchTeam() { Id = n };
            if (!TestTeamList.Contains(rt))
            {
                TestTeamList.Add(rt as Team);
                TestStringList.Add(rt.ToString());
                TestTeamDictionary.Add(rt as Team, rt);
                TestStringDictionary.Add(rt.ToString(), rt);
            }
            return rt;
        }
        public TestCollections(int n)
        {
            for(int i = 1; i <= n; i++)
            {
                //для того, чтобы использовать метод TestStaticMethod, который все равно добавляет элементы
                ResearchTeam temp = TestStaticMethod(i);
            }
        }
        public string Time(int n)
        {
            ResearchTeam rt = new ResearchTeam() { Id = n };
            string info = "";
            bool temp;
            System.Diagnostics.Stopwatch time = new System.Diagnostics.Stopwatch();
            time.Start();
            Team temp1 = rt as Team;
            temp = TestTeamList.Contains(temp1);
            time.Stop();
            info += String.Format("Время поиска элемента в List<Team> - {0}\n", time.Elapsed);

            time.Reset();
            time.Start();
            string temp2 = rt.ToString();
            temp = TestStringList.Contains(temp2);
            time.Stop();
            info += String.Format("Время поиска элемента в List<string> - {0}\n", time.Elapsed);

            time.Reset();
            time.Start();
            temp = TestTeamDictionary.ContainsKey(temp1);
            time.Stop();
            info += String.Format("Время поиска по ключу в Dictionary<Team,ResearchTeam> - {0}\n", time.Elapsed);

            time.Reset();
            time.Start();
            temp = TestTeamDictionary.ContainsValue(rt);
            time.Stop();
            info += String.Format("Время поиска по значению в Dictionary<Team,ResearchTeam> - {0}\n", time.Elapsed);

            time.Reset();
            time.Start();
            temp = TestStringDictionary.ContainsKey(temp2);
            time.Stop();
            info += String.Format("Время поиска по ключу в Dictionary<string,ResearchTeam> - {0}\n", time.Elapsed);

            time.Reset();
            time.Start();
            temp = TestStringDictionary.ContainsValue(rt);
            time.Stop();
            info += String.Format("Время поиска по значению в Dictionary<string,ResearchTeam> - {0}\n", time.Elapsed);

            return info;
        }
        //статическиий конструктор, чтобы инициализировать статические методы
        static TestCollections()
        {
            TestTeamList = new List<Team>();
            TestStringList = new List<string>();
            TestTeamDictionary = new Dictionary<Team, ResearchTeam>();
            TestStringDictionary = new Dictionary<string, ResearchTeam>();
        }
    }
}